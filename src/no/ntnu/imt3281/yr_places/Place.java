package no.ntnu.imt3281.yr_places;

import java.util.HashMap;
import java.util.List;

public class Place {
	
	private HashMap<Integer, Object> data;
	
	public Place(List<String> list) {
		Integer key = 0;
		
		for(Object ea : list) {
			data.put(key, ea);
		}
	}

	public String getVarselURL() {
		return data.get(10).toString();
	}

	public double getLng() {
		return (double)data.get(9);
	}

	public double getLat() {
		return (double) data.get(8);
	}

	public String getFylke() {
		return data.get(7).toString();
	}

	public String getKommune() {
		return data.get(6).toString();
	}

	public String getStedstype() {
		return data.get(4).toString();
	}

	public String getStedsnavn() {
		return data.get(1).toString();
	}

	public int getKommunenr() {
		return (int) data.get(0);
	}
}
